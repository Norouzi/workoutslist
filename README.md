# Workouts List Demo

A Project prepared as a part of a job interview process

| Light Mode | Dark Mode |
| --- | --- | 
| <img src="LighMode.png" width=200>  | <img src="DarkMode.png" width=200> | 

### Some Notes
1. Based on the small amount of data currently avaiable on the `/trainers` endpoint, with no mention of `total_count` or `offset`, this solution assumes that all trainers can be fetched in one call.
2. The lazy loading solution references `scrollState.canScrollForward` to request more workouts. However, due to `workouts` and `loading` being updated separately, it currently does 2 calls at a time, which I couldn't fix yet!




