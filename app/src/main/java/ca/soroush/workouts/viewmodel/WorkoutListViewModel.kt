package ca.soroush.workouts.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ca.soroush.workouts.api.API
import ca.soroush.workouts.api.model.Workout
import ca.soroush.workouts.ui.views.WorkoutCardData
import ca.soroush.workouts.util.DateFormatter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class WorkoutListViewModel: ViewModel() {

    private val _workouts: MutableStateFlow<List<WorkoutCardData>> = MutableStateFlow(emptyList())
    val workouts: StateFlow<List<WorkoutCardData>> = _workouts

    private val _error = MutableStateFlow<String?>(null)
    val error: StateFlow<String?> = _error

    private val _loading = MutableStateFlow(false)
    val loading: StateFlow<Boolean> = _loading

    private val trainerIdToNameMap = mutableMapOf<Int, String>()
    private var offset = 0
    private var totalWorkouts: Int? = null

    private suspend fun fetchTrainers() {
        _loading.value = true
        try {
            val response = API.trainersService.getTrainers()
            response.items.forEach { trainerIdToNameMap[it.id] = "${it.firstName} ${it.lastName}" }
            _loading.value = false
        } catch (e: Exception) {
            _error.value = e.message
            _loading.value = false
        }
    }

    fun fetchMoreWorkouts() {
        viewModelScope.launch(Dispatchers.IO) {
            while (trainerIdToNameMap.isEmpty()) fetchTrainers()
            if (_workouts.value.size == totalWorkouts) return@launch
            try {
                _loading.value = true
                val response = API.workoutsService.getWorkouts(offset)
                totalWorkouts = response.total_count
                this@WorkoutListViewModel.offset += response.items.size
                _workouts.value += response.items.map { it.toWorkoutCardData()}
                _loading.value = false
            } catch (e: Exception) {
                _loading.value = false
                _error.value = e.message
            }
        }
    }

    private fun Workout.toWorkoutCardData() = WorkoutCardData(
        title = title,
        trainerName = trainerIdToNameMap[trainer_id] ?: "",
        dateCreated = DateFormatter.formatDate(added),
        imageUrl = preview_img_url
    )
}