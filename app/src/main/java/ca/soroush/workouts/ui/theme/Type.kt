package ca.soroush.workouts.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import ca.soroush.workouts.R

// Set of Material typography styles to start with

val Graphik = FontFamily(
    Font(R.font.graphik_regular, FontWeight.Normal),
    Font(R.font.graphik_medium, FontWeight.Medium)
)


val Typography = Typography(
    titleLarge = TextStyle(
        fontFamily = Graphik,
        fontWeight = FontWeight.Medium,
        fontSize = 26.sp,
        lineHeight = 40.sp,
    ),
    bodyLarge = TextStyle(
        fontFamily = Graphik,
        fontWeight = FontWeight.Medium,
        fontSize = 14.sp,
        lineHeight = 16.sp,
    ),
    labelSmall = TextStyle(
        fontFamily = Graphik,
        fontWeight = FontWeight.Normal,
        fontSize = 10.sp,
        lineHeight = 16.sp,
        letterSpacing = 0.5.sp
    )
)