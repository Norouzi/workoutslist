package ca.soroush.workouts.ui.views

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import ca.soroush.workouts.R
import ca.soroush.workouts.viewmodel.WorkoutListViewModel

@Composable
fun WorkoutList(viewModel: WorkoutListViewModel) {
    val workouts = viewModel.workouts.collectAsState().value
    val loading = viewModel.loading.collectAsState().value
    val error = viewModel.error.collectAsState().value

    val scrollState = rememberLazyListState()
    LaunchedEffect(!scrollState.canScrollForward) { viewModel.fetchMoreWorkouts() }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = workouts.size.toString() + " " + stringResource(id = R.string.app_name),
                style = typography.titleLarge,
                modifier = Modifier.padding(dimensionResource(id = R.dimen.card_padding))
            )
            if (loading) CircularProgressIndicator(
                modifier = Modifier.size(24.dp)
            )
        }
        if (error?.isNotBlank() == true) Text(error)

        LazyColumn(
            state = scrollState
        ) {
            items(count = workouts.size) {
                WorkoutCard(data = workouts[it])
            }
        }
    }
}
