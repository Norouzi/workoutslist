package ca.soroush.workouts.ui.views

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import ca.soroush.workouts.R
import ca.soroush.workouts.ui.theme.WorkoutsTheme
import coil.compose.AsyncImage

data class WorkoutCardData(
    val title: String,
    val trainerName: String,
    val dateCreated: String,
    val imageUrl: String
)

@Composable
fun WorkoutCard(data: WorkoutCardData) {
    Card(
        modifier = Modifier
            .widthIn(max = dimensionResource(id = R.dimen.card_max_width))
            .padding(dimensionResource(id = R.dimen.card_padding))
            .height(dimensionResource(id = R.dimen.card_height)),
        shape = RoundedCornerShape(dimensionResource(id = R.dimen.corner_radius_large)),
        elevation = CardDefaults.cardElevation(
            defaultElevation = dimensionResource(id = R.dimen.card_elevation)
        )
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .background(MaterialTheme.colorScheme.surfaceVariant)
        ) {
            Column(modifier = Modifier
                .padding(dimensionResource(id = R.dimen.card_padding))
                .widthIn(max = dimensionResource(id = R.dimen.card_text_width))) {
                Text(data.title, style = typography.bodyLarge)
                Text(
                    "${data.trainerName} · ${data.dateCreated}", style = typography.bodySmall
                )
            }
            AsyncImage(
                modifier = Modifier
                    .requiredWidth(dimensionResource(id = R.dimen.card_image_width))
                    .height(dimensionResource(id = R.dimen.card_height)),
                model = data.imageUrl,
                contentScale = ContentScale.FillBounds,
                contentDescription = null
            )
        }
    }
}

@Preview
@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun WorkoutCardPreview() {
    WorkoutsTheme {
        Surface {
            WorkoutCard(
                WorkoutCardData(
                    title = "5 Min Core Stabilization Workout",
                    trainerName = "John",
                    dateCreated = "2 days ago",
                    imageUrl = "https://i.pravatar.cc/300"
                )
            )

        }
    }
}
