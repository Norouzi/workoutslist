package ca.soroush.workouts.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import ca.soroush.workouts.ui.theme.WorkoutsTheme
import ca.soroush.workouts.ui.views.WorkoutList
import ca.soroush.workouts.viewmodel.WorkoutListViewModel

class MainActivity : ComponentActivity() {

    private val viewModel: WorkoutListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WorkoutsTheme {
                Surface(modifier = Modifier.fillMaxSize()) {
                    WorkoutList(viewModel)
                }
            }
        }
    }
}
