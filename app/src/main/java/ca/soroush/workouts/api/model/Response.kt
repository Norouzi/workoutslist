package ca.soroush.workouts.api.model

import com.google.gson.annotations.SerializedName

data class Response<T> (
    @SerializedName("items") val items: List<T>,
    @SerializedName("total_count") val total_count: Int,
    @SerializedName("offset") val offset: Int
)