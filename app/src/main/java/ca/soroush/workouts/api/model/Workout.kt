package ca.soroush.workouts.api.model

import com.google.gson.annotations.SerializedName

data class Workout (
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("added") val added: Int,
    @SerializedName("preview_img_url") val preview_img_url : String,
    @SerializedName("trainer_id") val trainer_id: Int
)