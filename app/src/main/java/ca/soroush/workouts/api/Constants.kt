package ca.soroush.workouts.api

const val BASE_URL = "https://android-trial.fightcamp.io/"

object Endpoints {
    const val Trainers = "trainers"
    const val Workouts = "workouts"
}