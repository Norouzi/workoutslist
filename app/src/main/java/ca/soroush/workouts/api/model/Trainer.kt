package ca.soroush.workouts.api.model

import com.google.gson.annotations.SerializedName

data class Trainer(
    @SerializedName("id") val id: Int,
    @SerializedName("first_name") val firstName: String,
    @SerializedName("last_name") val lastName: String,
)