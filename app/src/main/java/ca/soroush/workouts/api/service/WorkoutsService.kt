package ca.soroush.workouts.api.service

import ca.soroush.workouts.api.Endpoints
import ca.soroush.workouts.api.model.Response
import ca.soroush.workouts.api.model.Workout
import retrofit2.http.GET
import retrofit2.http.Query

interface WorkoutsService {
    @GET(Endpoints.Workouts)
    suspend fun getWorkouts(@Query("offset") offset: Int): Response<Workout>
}