package ca.soroush.workouts.api.service

import ca.soroush.workouts.api.Endpoints
import ca.soroush.workouts.api.model.Response
import ca.soroush.workouts.api.model.Trainer
import retrofit2.http.GET

interface TrainersService {
    @GET(Endpoints.Trainers)
    suspend fun getTrainers(): Response<Trainer>
}