package ca.soroush.workouts.api

import ca.soroush.workouts.api.service.TrainersService
import ca.soroush.workouts.api.service.WorkoutsService
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object API {
    private const val baseUrl = BASE_URL
    private val gson = GsonBuilder().setLenient().create()
    private val retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    val trainersService: TrainersService = retrofit.create(TrainersService::class.java)
    val workoutsService: WorkoutsService = retrofit.create(WorkoutsService::class.java)
}