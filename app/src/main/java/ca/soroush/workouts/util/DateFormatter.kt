package ca.soroush.workouts.util

import java.util.Date

object DateFormatter {
    fun formatDate(unixTime: Int): String {
        val date = Date(unixTime * 1000L)
        val formatter = java.text.SimpleDateFormat("MM/dd/yy")
        return formatter.format(date)
    }
}